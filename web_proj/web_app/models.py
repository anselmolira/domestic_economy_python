from django.db import models
from django.utils import timezone
import uuid


# Create your models here.
class Person(models.Model):
	name = models.CharField(max_length=120, null=False, db_column="Nome")
	birthdate = models.DateField(null=True, db_column="DataNascimento")
	email = models.CharField(max_length=120, null=False, db_column="Email")
	register_date = models.DateTimeField(null=False, default=timezone.now, editable=False, db_column="DataHoraRegistro")
	personKey = models.UUIDField(default=uuid.uuid4, editable=False, null=False, db_column="ChavePessoa")

	class Meta:
		db_table = "Pessoa"
		verbose_name = 'Pessoa'
        verbose_name_plural = 'Pessoas'
        ordering = ('register_date', )



class User(Person):
	def __str__(self):
		return self.name + ' - ' + self.email

	login = models.CharField(max_length=60, null=False, db_column="Login")
	password = models.CharField(max_length=20, null=False, db_column="Senha")

	class Meta:
		db_table = "Usuario"
		verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'
        ordering = ('login', )



class ProductCategory(models.Model):
	def __str__(self):
		return self.name

	name = models.CharField(max_length=100, null=False, db_column="Nome")
	description = models.TextField(null=True, db_column="Descricao")

	class Meta:
		db_table = "CategoriaProduto"


class ProductType(models.Model):
	def __str__(self):
		return self.category.name + ' - ' + self.name

	name = models.CharField(max_length=100, null=False, db_column="Nome")
	description = models.TextField(null=True, db_column="Descricao")
	category = models.ForeignKey(ProductCategory, db_column="IdCategoria")
	productTypeKey = models.UUIDField(default=uuid.uuid4, editable=False, null=False, db_column="ChaveTipoProduto")

	class Meta:
		db_table = "TipoProduto"


class ProductBrand(models.Model):
	def __str__(self):
		return self.name

	name = models.CharField(max_length=100, null=False, db_column="Nome")
	description = models.CharField(max_length=3000, null=True, db_column="Descricao")
	productType = models.ForeignKey(ProductType, db_column="IdTipoProduto")

	class Meta:
		db_table = "MarcaProduto"


class Budget(models.Model):
	def __str__(self):
		return self.entryType + ' - ' + self.itemName

	ENTRY_TYPE = (
		('R', 'Receita'),
		('D', 'Despesa')
	)
	# Nome do ítem
	itemName = models.CharField(max_length=150, null=False)
	# Valor do ítem
	itemValue = models.DecimalField(max_digits=6, decimal_places=2, null=False)
	# Parcela corrente
	currentInstallment = models.IntegerField(null=True, default=0)
	# Total de parcelas
	totalInstallmentNumber=models.IntegerField(null=True, default=0)
	# Data da ocorrência
	entryDate = models.DateField(null=False)
	# Tipo da ocorrência (se receita ou despesa)
	entryType = models.CharField(max_length=1, choices=ENTRY_TYPE)
	# Data e hora do registro no banco
	registerDateTime = models.DateTimeField(null=False, default=timezone.now, editable=False)

	class Meta:
		db_table = "Orcamento"


class DomesticInventory(models.Model):
	def __str__(self):
		return self.itemType.itemName + ' - ' + self.name

	name = models.CharField(max_length=100, null=False, db_column="Nome")
	itemType = models.ForeignKey(ProductCategory, on_delete=models.SET_NULL, null=True, db_column="Tipo")
	quantity = models.IntegerField(null=False, default=0, db_column="QuantidadeEmEstoque")
	quantityPerMonth = models.IntegerField(null=False, default=1, db_column="QuantidadePorMes")
	comments = models.TextField(null=True, db_column="Comentarios")
	registerDateTime = models.DateTimeField(null=False, default=timezone.now, editable=False, db_column="DataHoraRegistro")

	class Meta:
		db_table = "DispensaDomestica"