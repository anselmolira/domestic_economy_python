from django.contrib import admin
from .models import Budget
from .models import DomesticInventory
from .models import Person
from .models import ProductBrand
from .models import ProductCategory
from .models import ProductType
from .models import User



# Register your models here.
admin.site.register(User)
admin.site.register(ProductCategory)
admin.site.register(ProductType)
admin.site.register(ProductBrand)
admin.site.register(Budget)
admin.site.register(DomesticInventory)

admin.site.site_header = 'Economia Domestica - Admin'